var http = require('http');

http.createServer(function (request, response) {
  response.writeHead(200, {'Content-Type': 'text/html'});
  response.end('<!DOCTYPE html><html><head></head><body><div>Reply from remote web server</div></body></html>');
  console.log('Received a request');
}).listen(80);

console.log('Web server running at http://192.168.0.10/');